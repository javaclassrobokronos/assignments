//Try to name all the parts of this class.

package myPackage;

public class ExampleCode {
	
	private example1.BarnYard _by;
	
	public ExampleCode(){
		_by = new example1.BarnYard;
		addTwoChickens();
	}
	
	public void addTwoChickens(){
		example1.Chicken() c1;
		c1 = new example1.Chicken();
		example1.Chicken() c2;
		c2 = new example1.Chicken();
		_by.addChicken(c1);
		_by.addChicken(c2);
		c1.start();
		c2.start();
	}
}
