package graphicsExampleCode;

import java.awt.event.ActionListener;
//This is an example Handler that will need to be added to a button
//Currently, it does nothing
//Remember, each Button can only have one ActionListener, but a program 
//with multiple buttons can have multiple Handler Classes to assign to different
//buttons(HINT HINT)
public class ExampleHandler implements ActionListener {
	
	@Override
	public void actionPerformed(ActionEvent e){
		
	}
}
