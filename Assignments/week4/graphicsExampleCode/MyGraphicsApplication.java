package graphicsExampleCode;

import javax.swing.JFrame;
// THIS IS MERELY THE OUTLINE OF A CLASS THAT CONTROLS A WINDOW
// YOUR ASSIGNMENT WILL REQUIRE YOU TO EXPAND ON THIS FRAMEWORK
// AGAIN, DO NOT SIMPLY COPY AND PASTE THIS CODE
public class MyGraphicsApplication {
	private JFrame _frame;
	public MyGraphicsApplication(){
		_frame = new JFrame("Title");
		_frame.pack();
		_frame.setVisible(true);
	}
	// Maybe add some methods here so your Handlers can interact with the graphics?
	
}
