package examples;

public class Closet {
	private Shirt _shirt;
	private Person _person;
	private Shirt _currentShirt;
	public Closet(){
		_shirt = new Shirt();
		//The above line is an example of composition
		//This is an imperfect example, as a Closet most likely would not produce a Person.
		//It is used here to make the association example more simple.
		_person = new Person(_shirt);
		//The above line is an example of association
		//_shirt is being "passed as an argument" to the new Person object
		Shirt s1;
		s1 = new Shirt();
		_person.setShirt(s1);
		_currentShirt = _person.getShirt();
		// _person.getShirt() evaluates to the Shirt object that is returned by getShirt().
		//In effect, _currentShirt would now hold a reference to the object that was held in _shirt of the Person class.
	}
}
