package examples;

public class Person {
	private Shirt _shirt;
	public Person(Shirt s){
		//The shirt object is implicitely assigned to the local variable s,
		//defined in the above parameter list.
		_shirt = s;
		//The shirt object, whose reference is held in s, is 
		//then assigned to _shirt instance variable.
		setShirt(_shirt);
		
	}
	public void setShirt(Shirt s){ //s = s1; (this is done automatically when called)
		_shirt = s;
	}
	public Shirt getShirt(){
		return _shirt;
		//Whatever object that is held in _shirt is returned.
		example1.BarnYard by; // <- code not run because method exits at return
	}
}
